package indah.w.uts_012_008_yudha_riska

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
       when(v?.id){
           R.id.btnLogin ->{
               var email : String = edUserName.text.toString()
               var password : String = edPassword.text.toString()

               if(email.isEmpty() || password.isEmpty()) {
                   Toast.makeText(this, "Username / Password can't be empty", Toast.LENGTH_LONG).show()
               }else{
                   val progressDialog = ProgressDialog (this)
                   progressDialog.isIndeterminate = true
                   progressDialog.setMessage("Authenticating...")
                   progressDialog.show()

                   FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                       .addOnCompleteListener {

                       }
                       .addOnFailureListener {
                           progressDialog.hide()
                           Toast.makeText(this,
                               "Username / Password incorrect", Toast.LENGTH_SHORT).show()
                       }
               }
           }
           R.id.txSignUp ->{
               var intent = Intent(this,SignUpActivity::class.java)
               startActivity(intent)
           }

       }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnLogin.setOnClickListener(this)
        txSignUp.setOnClickListener(this)

    }


}